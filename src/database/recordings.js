'use strict';

const Recordings = (sequelize, DataTypes) => {
	return sequelize.define('Recordings', {
		reference: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing reference'}}
		},
		username: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing username'}},
			allowNull: false
        },
        flight_ref: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing recording reference'}},
			allowNull: false
		},
		booking_ref: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing recording reference'}},
			allowNull: false
		}
	});
};

module.exports = Recordings;
