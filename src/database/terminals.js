'use strict';

const Terminals = (sequelize, DataTypes) => {
	return sequelize.define('Terminals', {
		reference: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing reference'}}
		},
		name: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing name'}},
			allowNull: false
		},
		type: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing type'}},
			allowNull: false
		},
		flights_ref: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing flights_ref'}},
			allowNull: false
		}
	});
};

module.exports = Terminals;
