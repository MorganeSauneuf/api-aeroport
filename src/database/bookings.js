'use strict';

const Bookings = (sequelize, DataTypes) => {
	return sequelize.define('Bookings', {
		reference: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing reference'}}
		},
		username: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing username'}},
			allowNull: false
        },
        flight_ref: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing flight reference'}},
			allowNull: false
		}
	});
};

module.exports = Bookings;
