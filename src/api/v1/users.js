const express = require('express');
const request = require('request-promise');
const router = express.Router();

/* GET users listing. */

router.get('/', async (req, res) => {
	const {country, company} = req.query;
	const filter = {
		where: {}
	};
	if (country) filter.where.country = country;
	if (company) filter.where.company = company;

	const {Users} = req.db;
	const users = await Users.findAll(filter);

	if (users.length > 0) {
		res.status(200).send(users);
	} else {
		res.status(404).send({message: "Not found"});
	}
});

router.post('/', async(req, res) => {
	try {
		const body = req.body;
		if (body.username && body.fullname && body.country && body.avatar && body.company && body.type) {
			const {Users} = req.db;
			const user = await Users.create(body);
			return res.status(201).send(user);
		}
		else {
			return res.status(400).send({message: 'Missing data'});
		}	
	} catch (err) {
		if (err.name === 'SequelizeUniqueConstraintError') {
			return res.status(409).send({message: 'User existe déjà'})
		}
	}
});

router.get('/:username', async (req, res) => {
	const username = req.params.username;
	const {Users} = req.db;
	const user = await Users.findOne({ where: {username: username} });
	if (user) {
		return res.send(user);
	} else {
		return res.status(404)
			.send({message: `Username ${username} not found`});
	}
});

router.put('/:username', async (req, res) => {
	const username = req.params.username;
	const body = req.body;
	const {Users} = req.db;
	const user = await Users.findOne({ where: {username: username} });
	if (user) {
		user.update(body);
		return res.send(user);
	} else {
		return res.status(404)
			.send({message: `User ${username} not found`});
	}
});


router.delete('/:username', async (req, res) => {
	const username = req.params.username;
	const { Users } = req.db;
	const user = await Users.findOne({ where: { username: username } });
	if (user) {
		user.destroy().then((destroyedUser) => {
			if (destroyedUser == 0) {
				res.status(404).send({ message: "The requesuser with username " + username + " could not be deleted. You may try another username." });
			} else {
				res.status(204).send({
					success: destroyedUser,
					description: "User with username " + username + " is deleted."
				});
			}
		});
	} else {
		res.status(404).send({ message: "The requested User with username " + username + " could not be deleted. You may try another username." });
	}
});

module.exports = router;
