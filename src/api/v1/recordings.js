const express = require('express');
// const {BadRequest} = require('http-errors');
const router = express.Router();

router.get('/', async (req, res) => {
	const {reference, username, flight_ref, booking_ref} = req.query;
	const filter = {
		where: {}
	};
	if (reference) filter.where.reference = reference;
	if (username) filter.where.username = username;
    if (flight_ref) filter.where.flight_ref = flight_ref;
    if (booking_ref) filter.where.booking_ref = booking_ref;

	const {Recordings} = req.db;
	const recordings = await Recordings.findAll(filter);

	if (recordings.length > 0) {
		res.status(200).send(recordings);
	} else {
		res.status(404).send({message: "Not found"});
	}
});

router.post('/', async (req, res) => {
    try {
        const body = req.body;
        if (body.reference && body.username && body.flight_ref && body.booking_ref) {
            const { Recordings } = req.db;
            const recording = await Recordings.create(body);
            return res.status(201).send(recording);
        }
        else return res.status(400).send({ message: 'Missing data' });
    } catch (err) {
        if (err.name === 'SequelizeUniqueConstraintError') {
            return res.status(409).send({ message: 'La réservation existe déjà' })
        }
    }
});

router.get('/:reference', async (req, res) => {
	const reference = req.params.reference;
	const {Recordings} = req.db;
	const recording = await Recordings.findOne({ where: {reference: reference} });
	if (recording) {
		return res.send(recording);
	} else return res.status(404).send({message: `Reference ${reference} not found`});
});

router.put('/:reference', async (req, res) => {
    const reference = req.params.reference;
    const { Recordings } = req.db;
    const recording = await Recordings.findOne({ where: { reference: reference } });
    if (recording) {
        recording.updateAttributes(req.body).then((updatedRecording) => {
            res.status(200).send(updatedRecording);
        });
    } else return res.status(404).send({ message: `Reference ${reference} not found` });
});

router.delete('/:reference', async (req, res) => {
	const reference = req.params.reference;
	const { Recordings } = req.db;
	const recording = await Recordings.findOne({ where: { reference: reference } });
	if (recording) {
		recording.destroy().then((destroyedRecording) => {
			if (destroyedRecording == 0) {
				res.status(404).send({ message: "The requesuser with recording " + recording + " could not be deleted. You may try another recording." });
			} else {
				res.status(204).send({
					success: destroyedRecording,
					description: "User with recording " + recording + " is deleted."
				});
			}
		});
	} else res.status(404).send({ message: "The requested User with recording " + recording + " could not be deleted. You may try another recording." });
});


module.exports = router;