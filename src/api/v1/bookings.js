const express = require('express');
// const {BadRequest} = require('http-errors');
const router = express.Router();

router.get('/', async (req, res) => {
	const {reference, username, flight_ref} = req.query;
	const filter = {
		where: {}
	};
	if (reference) filter.where.reference = reference;
	if (username) filter.where.username = username;
	if (flight_ref) filter.where.flight_ref = flight_ref;

	const {Bookings} = req.db;
	const bookings = await Bookings.findAll(filter);

	if (bookings.length > 0) {
		res.status(200).send(bookings);
	} else {
		res.status(404).send({message: "Not found"});
	}
});

router.post('/', async (req, res) => {
    try {
        const body = req.body;
        if (body.reference && body.username && body.flight_ref) {
            const { Bookings } = req.db;
            const booking = await Bookings.create(body);
            return res.status(201).send(booking);
        }
        else return res.status(400).send({ message: 'Missing data' });
    } catch (err) {
        if (err.name === 'SequelizeUniqueConstraintError') {
            return res.status(409).send({ message: 'La réservation existe déjà' })
        }
    }
});

router.get('/:reference', async (req, res) => {
	const reference = req.params.reference;
	const {Bookings} = req.db;
	const booking = await Bookings.findOne({ where: {reference: reference} });
	if (booking) {
		return res.send(booking);
	} else return res.status(404).send({message: `Reference ${reference} not found`});
});

router.put('/:reference', async (req, res) => {
	const reference = req.params.reference;
	const body = req.body;
	const {Bookings} = req.db;
	const booking = await Bookings.findOne({ where: {reference: reference} });
	if (booking) {
		booking.update(body);
		return res.send(booking);
	} else return res.status(404).send({message: `Booking ${reference} not found`});
});

router.delete('/:reference', async (req, res) => {
	const reference = req.params.reference;
	const { Bookings } = req.db;
	const booking = await Bookings.findOne({ where: { reference: reference } });
	if (booking) {
		booking.destroy().then((destroyedBooking) => {
			if (destroyedBooking == 0) {
				res.status(404).send({ message: "The requesuser with booking " + booking + " could not be deleted. You may try another booking." });
			} else {
				res.status(204).send({
					success: destroyedBooking,
					description: "User with booking " + booking + " is deleted."
				});
			}
		});
	} else res.status(404).send({ message: "The requested User with booking " + booking + " could not be deleted. You may try another booking." });
});


module.exports = router;