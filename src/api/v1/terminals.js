const express = require('express');
const {BadRequest} = require('http-errors');
const router = express.Router();

router.get('/', async (req, res) => {
	const {reference, name, type, flights_ref} = req.query;
	const filter = {
		where: {}
	};
	if (reference) filter.where.reference = reference;
	if (name) filter.where.name = name;
    if (type) filter.where.type = type;
    if (flights_ref) filter.where.flights_ref = flights_ref;

	const {Terminals} = req.db;
	const terminals = await Terminals.findAll(filter);

	if (terminals.length > 0) {
		res.status(200).send(terminals);
	} else {
		res.status(404).send({message: "Not found"});
	}
});

router.post('/', async (req, res) => {
	try {
		const {body: givenTerminal} = req;
		const {Terminals} = req.db;
		const terminal = await Terminals.create(givenTerminal);
		res.status(201).send(terminal);
	} catch(err) {
		if (err.name === 'SequelizeValidationError') {
			res.status(400).send({ message: "Missing data"});
		} else if (err.name === 'SequelizeUniqueConstraintError') {
			res.status(409).send({ message: "The given terminal already exists in database"});
		}
	}
});

router.get('/:reference', async (req, res) => {
	const reference = req.params.reference;
	const {Terminals} = req.db;
	const terminal = await Terminals.findOne({ where: {reference: reference} });
	if (terminal) {
		return res.status(200).send(terminal);
	} else {
		return res.status(404)
			.send({message: `Reference ${reference} not found`});
	}
});

router.put('/:reference', async (req, res) => {
	const reference = req.params.reference;
	const body = req.body;
	const {Terminals} = req.db;
	const terminal = await Terminals.findOne({ where: {reference: reference} });
	if (terminal) {
		terminal.update(body);
		return res.send(terminal);
	} else {
		return res.status(404)
			.send({message: `Terminal ${reference} not found`});
	}
 });

router.delete('/:reference', async (req, res) => {
    const reference = req.params.reference;
    const { Terminals } = req.db;
    const terminal = await Terminals.findOne({ where: { reference: reference } });
    if (terminal) {
        terminal.destroy().then((destoryedCount) => {
            if (destoryedCount == 0) {
                res.status(404).send({ message: "The requesTerminal with reference " + reference + " could not be deleted. You may try another reference." });
            } else {
                res.status(204).send({
                    success: destoryedCount,
                    description: "Terminal with reference " + reference + " is deleted."
                });
            }
        });
    } else {
        res.status(404).send({ message: "The requested Terminal with reference " + reference + " could not be deleted. You may try another reference." });
    }
});


module.exports = router;