const express = require('express');
const {BadRequest} = require('http-errors');
const router = express.Router();

router.get('/', async (req, res) => {
	const {origin, destination, company} = req.query;
	const filter = {
		where: {}
	};
	if (origin) filter.where.origin = origin;
	if (destination) filter.where.destination = destination;
	if (company) filter.where.company = company;

	const {Flights} = req.db;
	const flights = await Flights.findAll(filter);

	if (flights.length > 0) {
		res.status(200).send(flights);
	} else {
		res.status(404).send({message: "Not found"});
	}
});

router.post('/', async (req, res) => {
	try {
		const {body: givenFlight} = req;
		const {Flights} = req.db;
		const flight = await Flights.create(givenFlight);
		res.status(201).send(flight);
	} catch(err) {
		if (err.name === 'SequelizeValidationError') {
			res.status(400).send({ message: "Missing data"});
		} else if (err.name === 'SequelizeUniqueConstraintError') {
			res.status(409).send({ message: "The given flight already exists in database"});
		}
	}
});

router.get('/:reference', async (req, res) => {
	const reference = req.params.reference;
	const {Flights} = req.db;
	const flight = await Flights.findOne({ where: {reference: reference} });
	if (flight) {
		return res.status(200).send(flight);
	} else {
		return res.status(404).send({message: `Reference ${reference} not found`});
	}
});

router.put('/:reference', async (req, res) => {
	const reference = req.params.reference;
	const body = req.body;
	const {Flights} = req.db;
	const flight = await Flights.findOne({ where: {reference: reference} });
	if (flight) {
		flight.update(body);
		return res.status(200).send(flight);
	} else {
		return res.status(404).send({message: `Flight ${reference} not found`});
	}
 });

router.delete('/:reference', async (req, res) => {
    const reference = req.params.reference;
    const { Flights } = req.db;
    const flight = await Flights.findOne({ where: { reference: reference } });
    if (flight) {
        flight.destroy().then((destoryedCount) => {
            if (destoryedCount == 0) {
                res.status(404).send({ message: "The requesFlight with reference " + reference + " could not be deleted. You may try another reference." });
            } else {
                res.status(204).send({
                    success: destoryedCount,
                    description: "Flight with reference " + reference + " is deleted."
                });
            }
        });
    } else {
        res.status(404).send({ message: "The requested Flight with reference " + reference + " could not be deleted. You may try another reference." });
    }
});


module.exports = router;