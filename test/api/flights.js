'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/flights')

const server = request(createServer());

describe('Flights api', function() {
	before(async function() {
		await database.sequelize.query('DELETE from FLIGHTS');
		const {Flights} = database;
		const promises = fixtures.map(flight => Flights.create(flight));
		await Promise.all(promises);
	});

	describe('GET /api/v1/flights', function() {
		it('On récupère et on affiche tous les vols', async () => {
			const {body: flights} = await server
				.get('/api/v1/flights')
				.set('Accept', 'application/json')
				.expect(200);
			expect(flights).to.be.an('array');
			expect(flights.length).to.equal(3);
		});
		it('On filtre et on affiche les vols', async () => {
			const {body: flights} = await server
				.get('/api/v1/flights')
				.query({
					company: 'Lufthansa',
					origin: 'CDG',
					destination: 'BOG'
				})
				.set('Accept', 'application/json')
				.expect(200);
			expect(flights).to.be.an('array');
			expect(flights.length).to.equal(1);
			expect(flights[0].reference).to.equal('LH-123')
		});
		it("Le filtre utilisé n’existe pas donc on n’affiche rien", async () => {
			await server
				.get('/api/v1/flights')
				.query({
					company: 'EasyJet'
				})
				.set('Accept', 'application/json')
				.expect(404);
		});
	});

	describe('POST /api/v1/flights', function() {
		it("On crée un vol", async () => {
			const {body: flight} = await server
				.post('/api/v1/flights')
				.set('Accept', 'application/json')
				.send({
					reference: 'AF-124',
					origin: 'CDG',
					destination: 'MAD',
					date: '2019-01-01',
					placesLeft: 30,
					price: 300.87,
					company: 'Air France'
				})
				.expect(201);
			expect(flight).to.be.an('object');
			expect(flight.reference).to.equal('AF-124');
		});	
		it("Il manque des informations pour créer le vol donc il n’est pas créé", async () => {
			await server
				.post('/api/v1/flights')
				.set('Accept', 'application/json')
				.send({
					placesLeft: 30,
					price: 300.87,
					company: 'Air France'
				})
				.expect(400);
		});
		it("Le vol existe déjà dans la base de données donc on ne peut pas le créer", async () => {
			await server.post('/api/v1/flights')
				.send({
					reference: "AF-123",
					origin: "CDG",
					destination: "BOG",
					date: "2019-01-01",
					placesLeft: 30,
					price: 200,
					company: "Air France"
				})
				.expect(409);
        });
	});

	describe('GET /api/v1/flights/:reference', function() {
		it("La référence donnée existe donc on affiche le vol", async () => {
			const {body: flight} = await server.get('/api/v1/flights/AF-123')
				.expect(200);
			expect(flight.reference).to.equal('AF-123');
			expect(flight.origin).to.equal('CDG');
		});
		it("La référence donnée n’existe pas donc on n’affiche rien", async () => {
			await server.get('/api/v1/flights/je-n-existe-pas')
				.expect(404);
		});
	});

	describe('PUT /api/v1/flights/:reference', function() {
		it("La référence donnée existe donc on modifie le vol trouvé", async () => {
			const {body: flight} = await server.put('/api/v1/flights/AF-123')
				.send({
					origin: "ORLY",
				})
				.expect(200);
			expect(flight.reference).to.equal('AF-123');
			expect(flight.origin).to.equal('ORLY');
		});
		it("La référence donnée n’existe pas donc on ne peut rien modifier", async () => {
			await server.put('/api/v1/flights/je-n-existe-pas')
				.expect(404);
		});
	});

	describe('DELETE /api/v1/flights/:reference', function() {
		it("La référence donnée existe donc on supprime le vol trouvé", async () => {
			await server.delete('/api/v1/flights/AF-123')
				.expect(204);
		});
		it("La référence donnée n’existe pas donc on ne peut rien supprimer", async () => {
			await server.delete('/api/v1/flights/je-n-existe-pas')
				.expect(404);
		});
	});
});