'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/terminals')

const server = request(createServer());

describe('Terminals api', function() {
	before(async function() {
		await database.sequelize.query('DELETE from TERMINALS');
		const {Terminals} = database;
		const promises = fixtures.map(terminal => Terminals.create(terminal));
		await Promise.all(promises);
	});

	describe('GET /api/v1/terminals', function() {
		it('On récupère et on affiche tous les terminaux', async () => {
			const {body: terminals} = await server
				.get('/api/v1/terminals')
				.set('Accept', 'application/json')
				.expect(200);
			expect(terminals).to.be.an('array');
			expect(terminals.length).to.equal(3);
		});
		it('On filtre et on affiche les terminaux', async () => {
			const {body: terminals} = await server
				.get('/api/v1/terminals')
				.query({
					type: 'europe'
				})
				.set('Accept', 'application/json')
				.expect(200);
			expect(terminals).to.be.an('array');
			expect(terminals.length).to.equal(2);
			expect(terminals[0].reference).to.equal('1')
		});
		it("Le filtre utilisé n’existe pas donc on n’affiche rien", async () => {
			await server
				.get('/api/v1/terminals')
				.query({
					type: 'zkf'
				})
				.set('Accept', 'application/json')
				.expect(404);
		});
	});

	describe('POST /api/v1/terminals', function() {
		it("On crée un terminal", async () => {
			const {body: terminal} = await server
				.post('/api/v1/terminals')
				.set('Accept', 'application/json')
				.send({
					reference: "4",
                    name: "B2",
                    type: "long courrier",
                    flights_ref: "SY-592, VU-603",
                    createdAt: "2019-02-11T16:44:26.180Z",
                    updatedAt: "2019-02-11T16:44:26.180Z"
				})
				.expect(201);
			expect(terminal).to.be.an('object');
			expect(terminal.reference).to.equal('4');
		});	
		it("Il manque des informations pour créer le terminal donc il n’est pas crée", async () => {
			await server
				.post('/api/v1/terminals')
				.set('Accept', 'application/json')
				.send({
					name: "B2",
                    type: "long courrier"
				})
				.expect(400);
		});
		it("Le terminal existe déjà dans la base de données donc on ne peut pas le créer", async () => {
			await server.post('/api/v1/terminals')
				.send({
					reference: "1",
                    name: "A1",
                    type: "europe",
                    flights_ref: "GH-961, AF-123",
                    createdAt: "2019-02-11T16:44:26.180Z",
                    updatedAt: "2019-02-11T16:44:26.180Z"
				})
				.expect(409);
        });
	});

	describe('GET /api/v1/terminals/:reference', function() {
		it("La référence donnée existe donc on affiche le terminal", async () => {
			const {body: terminal} = await server.get('/api/v1/terminals/3')
				.expect(200);
			expect(terminal.reference).to.equal('3');
			expect(terminal.name).to.equal('B1');
		});
		it("La référence donnée n’existe pas donc on n’affiche rien", async () => {
			await server.get('/api/v1/terminals/je-n-existe-pas')
				.expect(404);
		});
	});

	describe('PUT /api/v1/terminals/:reference', function() {
		it("La référence donnée existe donc on modifie le terminal trouvé", async () => {
			const {body: terminal} = await server.put('/api/v1/terminals/3')
				.send({
					name: "C1",
				})
                .expect(200);
			expect(terminal.reference).to.equal('3');
			expect(terminal.name).to.equal('C1');
		});
		it("La référence donnée n’existe pas donc on ne peut rien modifier", async () => {
			await server.put('/api/v1/terminals/je-n-existe-pas')
				.expect(404);
		});
	});

	describe('DELETE /api/v1/terminals/:reference', function() {
		it("La référence donnée existe donc on supprime le terminal trouvé", async () => {
			await server.delete('/api/v1/terminals/2')
				.expect(204);
		});
		it("La référence donnée n’existe pas donc on ne peut rien supprimer", async () => {
			await server.delete('/api/v1/terminals/je-n-existe-pas')
				.expect(404);
		});
	});
});