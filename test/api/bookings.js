'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/bookings')

const server = request(createServer());

describe('Booking api', function() {
	before(async function() {
		await database.sequelize.query('DELETE from BOOKINGS');
		const {Bookings} = database;
		const promises = fixtures.map(booking => Bookings.create(booking));
		await Promise.all(promises);
	});

	describe('GET /api/v1/bookings', function() {
		it('On récupère et on affiche toutes les réservations', async () => {
			const {body: bookings} = await server
				.get('/api/v1/bookings')
				.set('Accept', 'application/json')
				.expect(200);
			expect(bookings).to.be.an('array');
			expect(bookings.length).to.equal(2);
		});
		it('On filtre et on affiche les réservations', async () => {
			const {body: bookings} = await server
				.get('/api/v1/bookings')
				.query({
					flight_ref: 'string'
				})
				.set('Accept', 'application/json')
				.expect(200);
			expect(bookings).to.be.an('array');
			expect(bookings.length).to.equal(1);
			expect(bookings[0].reference).to.equal('string')
		});
		it("Le filtre utilisé n’existe pas donc on n’affiche rien", async () => {
			await server
				.get('/api/v1/bookings')
				.query({
					flight_ref: 'kgss'
				})
				.set('Accept', 'application/json')
				.expect(404);
		});
	});

    describe('POST /api/v1/bookings', function() {
		it("On crée une réservation", async () => {
			const {body: booking} = await server
				.post('/api/v1/bookings')
				.set('Accept', 'application/json')
				.send({
					reference: "test",
                    username: "test",
                    flight_ref: "test",
				})
				.expect(201);
			expect(booking).to.be.an('object');
			expect(booking.reference).to.equal('test');
		});	
		it("Il manque des informations pour créer la réservation donc elle n’est pas créé", async () => {
			await server
				.post('/api/v1/bookings')
				.set('Accept', 'application/json')
				.send({
					reference: "string",
                    username: "string"
				})
				.expect(400);
		});

		it("La réservations existe déjà dans la base de données donc on ne peut pas la créer", async () => {
			await server
				.post('/api/v1/bookings')
				.send({
					reference: "string",
                    username: "string",
                    flight_ref: "string",
				})
				.expect(409);
        });
	});

    describe('GET /api/v1/bookings/:reference', function() {
		it("La référence donnée existe donc on affiche la réservation", async () => {
			const {body: booking} = await server.get('/api/v1/bookings/string')
				.expect(200);
			expect(booking.reference).to.equal('string');
			expect(booking.username).to.equal('string');
		});
		it("La référence donnée n’existe pas donc on n’affiche rien", async () => {
			await server.get('/api/v1/bookings/je-n-existe-pas')
				.expect(404);
		});
	});

	describe('PUT /api/v1/bookings/:reference', function() {
		it("La référence donnée existe donc on modifie la réservation trouvée", async () => {
			const {body: booking} = await server.put('/api/v1/bookings/string')
				.send({
					username: "string",
				})
                .expect(200);
			expect(booking.reference).to.equal('string');
			expect(booking.username).to.equal('string');
		});
		it("La référence donnée n’existe pas donc on ne peut rien modifier", async () => {
			await server.put('/api/v1/bookings/je-n-existe-pas')
				.expect(404);
		});
	});

	describe('DELETE /api/v1/bookings/:reference', function() {
		it("La référence donnée existe donc on supprime la réservation trouvée", async () => {
			await server.delete('/api/v1/bookings/string')
				.expect(204);
		});
		it("La référence donnée n’existe pas donc on ne peut rien supprimer", async () => {
			await server.delete('/api/v1/bookings/je-n-existe-pas')
				.expect(404);
		});
	});

});