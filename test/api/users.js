'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/users')

const server = request(createServer());

describe('User api', function() {
	before(async function() {
		await database.sequelize.query('DELETE from USERS');
		const {Users} = database;
		const promises = fixtures.map(user => Users.create(user));
		await Promise.all(promises);
	});

	describe('GET /api/v1/users', function() {
		it('On récupère et on affiche tous les users', async () => {
			const {body: users} = await server
				.get('/api/v1/users')
				.set('Accept', 'application/json')
				.expect(200);
			expect(users).to.be.an('array');
			expect(users.length).to.equal(2);
		});
		it('On filtre et on affiche les users', async () => {
			const {body: users} = await server
				.get('/api/v1/users')
				.query({
					country: 'FR'
				})
				.set('Accept', 'application/json')
				.expect(200);
			expect(users).to.be.an('array');
			expect(users.length).to.equal(2);
			expect(users[0].username).to.equal('johan')
		});
		it("Le filtre utilisé n’existe pas donc on n’affiche rien", async () => {
			await server
				.get('/api/v1/users')
				.query({
					country: 'kgss'
				})
				.set('Accept', 'application/json')
				.expect(404);
		});
	});

    describe('POST /api/v1/users', function() {
		it("On crée un user", async () => {
			const {body: user} = await server
				.post('/api/v1/users')
				.set('Accept', 'application/json')
				.send({
					username: "marco",
					fullname: "marco",
					country: "DEU",
					avatar: "avatar",
					company: "Dortmund",
					type: "person"
				})
				.expect(201);
			expect(user).to.be.an('object');
			expect(user.username).to.equal('marco');
		});	
		it("Il manque des informations pour créer le user donc il n’est pas créé", async () => {
			await server
				.post('/api/v1/users')
				.set('Accept', 'application/json')
				.send({
					username: "johan",
                    avatar: "avatar"
				})
				.expect(400);
		});
		it("Le user existe déjà dans la base de données donc on ne peut pas le créer", async () => {
			await server
				.post('/api/v1/users')
				.send({
					username: "johan",
					fullname: "johan",
					country: "FR",
					avatar: "avatar",
					company: "Apple",
					type: "person"
				})
				.expect(409);
        });
	});

    describe('GET /api/v1/users/:username', function() {
		it("Le username donné existe donc on affiche le user", async () => {
			const {body: user} = await server.get('/api/v1/users/johan')
				.expect(200);
			expect(user.username).to.equal('johan');
			expect(user.fullname).to.equal('johan');
		});
		it("Le username donné n’existe pas donc on n’affiche rien", async () => {
			await server.get('/api/v1/users/je-n-existe-pas')
				.expect(404);
		});
	});

	describe('PUT /api/v1/users/:username', function() {
		it("Le username donné existe donc on modifie le user trouvé", async () => {
			const {body: user} = await server.put('/api/v1/users/johan')
				.send({
					fullname: "johan",
				})
                .expect(200);
			expect(user.username).to.equal('johan');
			expect(user.fullname).to.equal('johan');
		});
		it("Le username donné n’existe pas donc on ne peut rien modifier", async () => {
			await server.put('/api/v1/users/je-n-existe-pas')
				.expect(404);
		});
	});

	describe('DELETE /api/v1/users/:username', function() {
		it("Le username donné existe donc on supprime le user trouvé", async () => {
			await server.delete('/api/v1/users/johan')
				.expect(204);
		});
		it("Le username donné n’existe pas donc on ne peut rien supprimer", async () => {
			await server.delete('/api/v1/users/je-n-existe-pas')
				.expect(404);
		});
	});

});