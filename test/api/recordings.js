'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/recordings')

const server = request(createServer());

describe('Booking api', function() {
	before(async function() {
		await database.sequelize.query('DELETE from RECORDINGS');
		const {Recordings} = database;
		const promises = fixtures.map(recording => Recordings.create(recording));
		await Promise.all(promises);
	});

	describe('GET /api/v1/recordings', function() {
		it('On récupère et on affiche tous les enregistrements', async () => {
			const {body: recordings} = await server
				.get('/api/v1/recordings')
				.set('Accept', 'application/json')
				.expect(200);
			expect(recordings).to.be.an('array');
			expect(recordings.length).to.equal(2);
		});
		it('On filtre et on affiche les enregistrements', async () => {
			const {body: recordings} = await server
				.get('/api/v1/recordings')
				.query({
					flight_ref: 'string'
				})
				.set('Accept', 'application/json')
				.expect(200);
			expect(recordings).to.be.an('array');
			expect(recordings.length).to.equal(1);
			expect(recordings[0].reference).to.equal('string')
		});
		it("Le filtre utilisé n’existe pas donc on n’affiche rien", async () => {
			await server
				.get('/api/v1/recordings')
				.query({
					flight_ref: 'kgss'
				})
				.set('Accept', 'application/json')
				.expect(404);
		});
	});

    describe('POST /api/v1/recordings', function() {
		it("On crée un enregistrement", async () => {
			const {body: recording} = await server
				.post('/api/v1/recordings')
				.set('Accept', 'application/json')
				.send({
					reference: "test",
                    username: "test",
                    flight_ref: "test",
                    booking_ref: "test"
				})
				.expect(201);
			expect(recording).to.be.an('object');
			expect(recording.reference).to.equal('test');
		});	
		it("Il manque des informations pour créer l’enregistrement donc il n’est pas créé", async () => {
			await server
				.post('/api/v1/recordings')
				.set('Accept', 'application/json')
				.send({
					reference: "string",
                    username: "string"
				})
				.expect(400);
		});

		it("L’enregistrement existe déjà dans la base de données donc on ne peut pas le créer", async () => {
			await server
				.post('/api/v1/recordings')
				.send({
					reference: "string",
                    username: "string",
                    flight_ref: "string",
                    booking_ref: "string"
				})
				.expect(409);
        });
	});

    describe('GET /api/v1/recordings/:reference', function() {
		it("La référence donnée existe donc on affiche l’enregistrement", async () => {
			const {body: recording} = await server.get('/api/v1/recordings/string')
				.expect(200);
			expect(recording.reference).to.equal('string');
			expect(recording.username).to.equal('string');
		});
		it("La référence donnée n’existe pas donc on n’affiche rien", async () => {
			await server.get('/api/v1/recordings/je-n-existe-pas')
				.expect(404);
		});
	});

	describe('PUT /api/v1/recordings/:reference', function() {
		it("La référence donnée existe donc on modifie l’enregistrement trouvé", async () => {
			const {body: recording} = await server.put('/api/v1/recordings/string')
				.send({
					username: "string",
				})
                .expect(200);
			expect(recording.reference).to.equal('string');
			expect(recording.username).to.equal('string');
		});
		it("La référence donnée n’existe pas donc on ne peut rien modifier", async () => {
			await server.put('/api/v1/recordings/je-n-existe-pas')
				.expect(404);
		});
	});

	describe('DELETE /api/v1/recordings/:reference', function() {
		it("La référence donnée existe donc on supprime l’enregistrement trouvé", async () => {
			await server.delete('/api/v1/recordings/string')
				.expect(204);
		});
		it("La référence donnée n’existe pas donc on ne peut rien supprimer", async () => {
			await server.delete('/api/v1/recordings/je-n-existe-pas')
				.expect(404);
		});
	});

});